#include "shape.h"
#include <math.h>

namespace ShapeLibrary {


Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _a = a;
    _b = b;

}

double Ellipse::Area() const
{
    return M_PI * _a * _b;
}

Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius)
{
    _a = radius;
    _b = radius;
}


Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3){
    /*setp1(p1);*/ _p1 = p1;
    /*setp2(p2);*/ _p2 = p2;
    /*setp3(p3);*/ _p3 = p3;
}

double Triangle::Area() const
{
    double a;
    double b;
    double c;
    double p;
    a = sqrt((_p1._x - _p2._x)*(_p1._x - _p2._x) + (_p1._y - _p2._y)*(_p1._y - _p2._y));
    b = sqrt((_p2._x - _p3._x)*(_p2._x - _p3._x) + (_p2._y - _p3._y)*(_p2._y - _p3._y));
    c = sqrt((_p1._x - _p3._x)*(_p1._x - _p3._x) + (_p1._y - _p3._y)*(_p1._y - _p3._y));
    p = (a + b + c) / 2;

    //ho usato la formula di Erone
    return sqrt(p * (p - a) * (p - b) * (p - c));
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(p1, p1, p1)
{
    _edge = edge;
}

double TriangleEquilateral::Area() const
{
    double p;
    p = (3 * _edge) / 2;
    //anche qui ho usato la formula di Erone
    return sqrt(p * (p - _edge) * (p - _edge) * (p - _edge));
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
}

double Quadrilateral::Area() const
{
    double a;
    double b;
    double c;
    double d;
    double p;
    a = sqrt((_p1._x - _p2._x)*(_p1._x - _p2._x) + (_p1._y - _p2._y)*(_p1._y - _p2._y));
    b = sqrt((_p2._x - _p3._x)*(_p2._x - _p3._x) + (_p2._y - _p3._y)*(_p2._y - _p3._y));
    c = sqrt((_p3._x - _p4._x)*(_p3._x - _p4._x) + (_p3._y - _p4._y)*(_p3._y - _p4._y));
    d = sqrt((_p1._x - _p4._x)*(_p1._x - _p4._x) + (_p1._y - _p4._y)*(_p1._y - _p4._y));
    p = (a + b + c + d) / 2;

    //qui ho usato la formula di Brahmagupta che è simile a quella di Erone per i triangoli
    return sqrt((p - a) * (p - b) * (p - c) * (p - d));
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1, p1, p2, p4)
{
    _p1 = p1;
    _p2 = p2;
    _p4 = p4;
}

double Parallelogram::Area() const
{
    double base;
    double altezza_num;
    double altezza;
    base = sqrt((_p1._x - _p2._x)*(_p1._x - _p2._x) + (_p1._y - _p2._y)*(_p1._y - _p2._y));
    altezza_num = fabs(((_p1._y - _p2._y) * _p4._x) + ((_p2._x - _p1._x) * _p4._y) + (_p2._y * _p1._x) - (_p1._y * _p2._x));
    altezza = altezza_num / base;

    return base * altezza;
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1, p1, p1)
{
    _base = base;
    _height = height;
}

double Rectangle::Area() const
{
    return _base * _height;
}

Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge)
{
    _edge = edge;
}

double Square::Area() const
{
    return _edge * _edge;
}

}

