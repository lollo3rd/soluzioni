#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
//#include <cmath>

using namespace std;

namespace ShapeLibrary {

  class Point {

    public:
      double _x;
      double _y;

    public:
      Point(const double& x,      // potevo anche iniziallizare semplicemente le due variabili a x = 0.0 invece che mettere il costruttore vuoto
            const double& y);     // potevo anche iniziallizare semplicemente le due variabili a y = 0.0 invece che mettere il costruttore vuoto
      Point(){}
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      int _a;
      int _b;

    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);
      Ellipse(const Ellipse& ellipse) { }

      double Area() const;
  };

  class Circle : public Ellipse
  {      
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const { return Ellipse::Area(); }
  };


  class Triangle : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;
      int _edge;
    public:
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);
     // void setp1(const Point& p1){
     //     _p1 = p1;
     // }
     // void setp2(const Point& p2){
     //      _p2 = p2;
     // }
     // void setp3(const Point& p3){
     //      _p3 = p3;
     // }

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;
      int _base;
      int _height;
      int _edge;
    public:
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public Parallelogram
  {
    public:
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    public:
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
