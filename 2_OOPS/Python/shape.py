import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self._center = center
        self._a = a
        self._b = b
    def area(self) -> float:
        return math.pi * self._a * self._b


class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        self._a = radius
        self._b = radius

    def area(self) -> float:
        return math.pi * self._a * self._b


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
    def area(self) -> float:
        a = math.sqrt((self._p1.x - self._p2.x) * (self._p1.x - self._p2.x) + (self._p1.y - self._p2.y) *
                      (self._p1.y - self._p2.y))
        b = math.sqrt((self._p3.x - self._p2.x) * (self._p3.x - self._p2.x) + (self._p3.y - self._p2.y) *
                      (self._p3.y - self._p2.y))
        c = math.sqrt((self._p1.x - self._p3.x) * (self._p1.x - self._p3.x) + (self._p1.y - self._p3.y) *
                      (self._p1.y - self._p3.y))
        p = (a + b + c) / 2

        # qui ho usato la formula di Erone
        return math.sqrt(p * (p - a) * (p - b) * (p - c))


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        self.__edge = edge
    def area(self) -> float:
        p = (3 * self.__edge) / 2

        # qui ho usato sempre la formula di Erone
        return math.sqrt(p * (p - self.__edge) * (p - self.__edge) * (p - self.__edge))

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4
    def area(self) -> float:
        a = math.sqrt((self._p1.x - self._p2.x) * (self._p1.x - self._p2.x) + (self._p1.y - self._p2.y) *
                      (self._p1.y - self._p2.y))
        b = math.sqrt((self._p2.x - self._p3.x) * (self._p2.x - self._p3.x) + (self._p2.y - self._p3.y) *
                      (self._p2.y - self._p3.y))
        c = math.sqrt((self._p3.x - self._p4.x) * (self._p3.x - self._p4.x) + (self._p3.y - self._p4.y) *
                      (self._p3.y - self._p4.y))
        d = math.sqrt((self._p1.x - self._p4.x) * (self._p1.x - self._p4.x) + (self._p1.y - self._p4.y) *
                      (self._p1.y - self._p4.y))
        # qui ho usato la formula di Brahmagupta
        p = (a + b + c + d) / 2

        return math.sqrt((p - a) * (p - b) * (p - c) * (p - d))


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p4 = p4

    def area(self) -> float:
        base = math.sqrt((self._p1.x - self._p2.x) * (self._p1.x - self._p2.x) + (self._p1.y - self._p2.y) *
                         (self._p1.y - self._p2.y))
        altezza_num = abs(
            ((self._p1.y - self._p2.y) * self._p4.x) + ((self._p2.x - self._p1.x) * self._p4.y) +
            (self._p2.y * self._p1.x) - (self._p1.y * self._p2.x))
        altezza = altezza_num / base

        return base * altezza

class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        self.__base = base
        self.__height = height
    def area(self) -> float:
        return self.__base * self.__height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        self.__edge = edge
    def area(self) -> float:
        return self.__edge * self.__edge
