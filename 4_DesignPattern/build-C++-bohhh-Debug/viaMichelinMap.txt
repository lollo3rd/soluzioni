# Number of busStop
3
# Id Name Lat Lon
1 Cadorna 450781 76761
2 Politecnico 450791 76771
3 Castello 450771 76781
# Number of Streets
4
# Id From To TravelTime(s)
1 1 2 17
2 2 3 48
3 1 3 45
4 3 1 24
# Number of Routes
2
# Id NumberStreets StreetIds
1 3 1 2 4
2 2 3 4
