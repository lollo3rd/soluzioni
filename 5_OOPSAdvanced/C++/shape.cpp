#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*_a;
  }

  Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) {

      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
  }

  void Triangle::AddVertex(const Point &point) {

      points.push_back(point);
  }

  double Triangle::Perimeter() const
  {
      double perimeter = 0;
      if (points.size() > 1){
        double AB = 0, BC = 0, CA = 0;
        AB = sqrt((points[0].X - points[1].X) * (points[0].X - points[1].X) + (points[0].Y - points[1].Y) * (points[0].Y - points[1].Y));
        BC = sqrt((points[1].X - points[2].X) * (points[1].X - points[2].X) + (points[1].Y - points[2].Y) * (points[1].Y - points[2].Y));
        CA = sqrt((points[0].X - points[2].X) * (points[0].X - points[2].X) + (points[0].Y - points[2].Y) * (points[0].Y - points[2].Y));
        perimeter = AB + BC + CA;
      }
      else {
          perimeter = Edge*3;
      }
      return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge)
  {
        AddVertex(p1);
        Edge = edge;
  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
        AddVertex(p1);
        AddVertex(p2);
        AddVertex(p3);
        AddVertex(p4);

  }

  double Quadrilateral::Perimeter() const
  {
      double perimeter = 0;
      if (points.size() > 1){
          double AB = 0, BC = 0, CD = 0, DA = 0;
          AB = sqrt((points[0].X - points[1].X) * (points[0].X - points[1].X) + (points[0].Y - points[1].Y) * (points[0].Y - points[1].Y));
          BC = sqrt((points[1].X - points[2].X) * (points[1].X - points[2].X) + (points[1].Y - points[2].Y) * (points[1].Y - points[2].Y));
          CD = sqrt((points[2].X - points[3].X) * (points[2].X - points[3].X) + (points[2].Y - points[3].Y) * (points[2].Y - points[3].Y));
          DA = sqrt((points[0].X - points[3].X) * (points[0].X - points[3].X) + (points[0].Y - points[3].Y) * (points[0].Y - points[3].Y));

          perimeter = AB + BC + CD + DA;
      }
      else {
        if (Edge_2 != 0){
            perimeter = 2 * (Edge_1 + Edge_2);
        }
        else{
            perimeter = Edge_1 * 4;
        }
      }
      return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      AddVertex(p1);
      Edge_1 = base;
      Edge_2 = height;

  }

  Point::Point(const double &x, const double &y) {

      X = x;
      Y = y;
  }

  Point Point::operator+(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X + point.X;
      tempPoint.Y = Y + point.Y;
      return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point tempPoint(X,Y);
      tempPoint.X = X - point.X;
      tempPoint.Y = Y - point.Y;
      return tempPoint;
  }

  Point& Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;
  }

  Point& Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }

  Circle::Circle(const Point &center, const double &radius) {
      _center = center;
      _a = radius;
  }

  Square::Square(const Point &p1, const double &edge) {

      AddVertex(p1);
      Edge_1 = edge;
  }


}
