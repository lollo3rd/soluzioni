#ifndef INTERSECTOR2D2D_HPP
#define INTERSECTOR2D2D_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

class Intersector2D2D;

class Intersector2D2D
{
  public:
    enum TypeIntersection
    {
      NoIntersection = 0,
      Coplanar = 1,
      LineIntersection = 2
    };

  protected:
    TypeIntersection intersectionType;

    double toleranceParallelism;
    double toleranceIntersection;

    Vector3d pointLine;         //anche questo non capisco proprio a cosa serva se nei test non viene passato alcun punto
    Vector3d tangentLine;       //idem per questo

    Vector3d rightHandSide;     //venendo passati i valori "d" di ognuno dei due piani,
                                //e non i punti generatori del piano, non potendo fare l'operazione inversa
                                //del prodotto scalare, utilizzerò solamente i valori di traslazione dei due piani,
                                //e non il righthandside, i test passeranno ugualmente

    Matrix3d matrixNomalVector;
    double firstPlaneTranslation;
    double secondPlaneTranslation;

  public:
    Intersector2D2D();
    ~Intersector2D2D();

    void SetTolleranceIntersection(const double& _tolerance) { toleranceIntersection = _tolerance; }
    void SetTolleranceParallelism(const double& _tolerance) { toleranceParallelism = _tolerance; }

    const TypeIntersection& IntersectionType() const { return intersectionType; }
    const double& ToleranceIntersection() const { return toleranceIntersection; }
    const double& ToleranceParallelism() const { return toleranceParallelism; }
    const Vector3d& PointLine() const { return pointLine; }
    const Vector3d& TangentLine() const { return tangentLine; }

    void SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation);
    void SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation);
    bool ComputeIntersection();
};

#endif //INTERSECTOR2D2D_HPP

