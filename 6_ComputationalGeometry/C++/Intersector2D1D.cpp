#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = NoIntersection;
    intersectionParametricCoordinate = 0.0;

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;

}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;

}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    bool intersection = false;
    double denominatore = planeNormalPointer->dot(*lineTangentPointer);
    if (abs(denominatore) > toleranceIntersection){
        intersectionParametricCoordinate = (*planeTranslationPointer - planeNormalPointer->dot(*lineOriginPointer)) / denominatore;
        intersectionType = PointIntersection;
        intersection = true;
    }
    else {
        double check = *planeTranslationPointer - planeNormalPointer->dot(*lineOriginPointer);
        if (abs(check) > toleranceParallelism){
            intersectionParametricCoordinate = (*planeTranslationPointer - planeNormalPointer->dot(*lineOriginPointer)) / denominatore;
            intersectionType = NoIntersection;
        }
        else {
            intersectionParametricCoordinate = (*planeTranslationPointer - planeNormalPointer->dot(*lineOriginPointer)) / denominatore;
            intersectionType = Coplanar;
        }
    }
    return intersection;
}
