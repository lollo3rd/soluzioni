#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = NoIntersection;

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector(0,0) = 1.0;
    matrixNomalVector(0,1) = 1.0;
    matrixNomalVector(0,2) = 1.0;
    matrixNomalVector.row(1) = planeNormal;
    firstPlaneTranslation = planeTranslation;

}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(2) = planeNormal;
    secondPlaneTranslation = planeTranslation;

}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    intersectionType = NoIntersection;
    bool intersection = false;
    double perpendicularity = matrixNomalVector.determinant();
    if(abs(perpendicularity) > toleranceIntersection){
        intersectionType = LineIntersection;
        intersection = true;
    }
    else {
        double check = firstPlaneTranslation - secondPlaneTranslation;
        if(abs(check) > toleranceParallelism){
            intersectionType = NoIntersection;
        }
        else {
            intersectionType = Coplanar;
        }
    }
    return intersection;
}
