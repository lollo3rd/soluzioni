#include "Pizzeria.h"


namespace PizzeriaLibrary {

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {

    unsigned int numIngredient = NumberIngredients();
    bool repeated = false;
    Ingredient ingrediente;
    ingrediente.Name = name;
    ingrediente.Description = description;
    ingrediente.Price = price;
    for (unsigned int i = 0; i< numIngredient; i++){
        if ( name == ingredients[i].Name){
            repeated = true;
            break;
        }
    }
    if (repeated == true)
        throw runtime_error("Ingredient already inserted");
    else
        ingredients.push_back(ingrediente);
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const {

    unsigned int numIngredient = NumberIngredients();
    bool found = false;
    unsigned int i;
    for(i = 0; i < numIngredient; i++){
        if (name == ingredients[i].Name){
            found = true;
            break;
        }
    }
    if (found == false)
        throw runtime_error("Ingredient not found");
    else {
        return ingredients[i];
    }
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {

    unsigned int numPizze = NumberPizze();
    bool repeated = false;
    Pizza pizza;
    pizza.Name = name;
    unsigned int numIngredienti = ingredients.size();
    for (unsigned int i = 0; i < numIngredienti; i++){
        //Pizzeria::ingredients[i].Name = ingredients[i]; è inutile perchè contengono le stesse cose
        pizza.AddIngredient(Pizzeria::ingredients[i]);
    }
    for (unsigned int i = 0; i < numPizze; i++){
        if ( name == pizze[i].Name){
            repeated = true;
            break;
        }
    }
    if (repeated == true)
        throw runtime_error("Pizza already inserted");
    else
        pizze.push_back(pizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const {

    unsigned int numPizza = NumberPizze();
    bool found = false;
    unsigned int i;
    for(i = 0; i < numPizza; i++){
        if (name == pizze[i].Name){
            found = true;
            break;
        }
    }
    if (found == false)
        throw runtime_error("Pizza not found");
    else {
        return pizze[i];
    }
}

int Pizzeria::CreateOrder(const vector<string> &pizzas) {

    unsigned int numOrdini = NumberOrders();
    unsigned int j = 0;
    int number = 0;
    int numPizze = pizzas.size();
    bool empty = false;
    Order ordine;
    for (int i = 0; i < numPizze; i++){
        ordine.AddPizza(Pizzeria::pizze[i]);
    }
    if (numOrdini == 0)
        ordine.InitializeOrder(j);
    else{
        while (j != numOrdini) {
            j++;
        }
        ordine.InitializeOrder(j);
    }

    number = ordine.Client;
    orders.push_back(ordine);
    if (numPizze == 0)
        empty = true;
    if (empty == true)
        throw runtime_error("Empty order");
    else
        return number;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const {

    unsigned int numOrdini = NumberOrders();
    unsigned int i;
    for(i = 0; i < numOrdini; i++){
        if (numOrder == orders[i].Client){
            break;
        }
    }
    if (orders[i].Pizzas.size() == 0)
        throw runtime_error("Order not found");
    else {
        return orders[i];
    }
}

string Pizzeria::GetReceipt(const int &numOrder) const {

    if (orders[numOrder - 1000].Pizzas.size() == 0){
        throw runtime_error("Order not found");
    }
    else{
        string Receipt;
        string prezzo_pizza;
        string total = to_string(orders[numOrder - 1000].ComputeTotal());
        unsigned int numPizze = orders[numOrder - 1000].Pizzas.size();
        for (unsigned int i = 0; i < numPizze; i++){
            prezzo_pizza = to_string(orders[numOrder - 1000].Pizzas[i].ComputePrice());
            Receipt = Receipt + "- " + orders[numOrder - 1000].Pizzas[i].Name + ", " + prezzo_pizza + " euro" + "\n";
        }
        Receipt = Receipt + "  TOTAL: " + total + " euro" + "\n";
        return Receipt;
    }
}

string Pizzeria::ListIngredients() const {

    string prezzo;
    string ListaIngredienti;
    Ingredient ingrediente_temporaneo;
    vector<Ingredient> ingredienti_ordinati;
    ingredienti_ordinati = ingredients;
    unsigned int numIngredient = NumberIngredients();
    //ordino con il bubble-sort
    for (unsigned int i = 0; i < numIngredient; i++){
        for (unsigned int j = 0; j < numIngredient -i-1; j++){
            if (strcmp (ingredienti_ordinati[j].Name.c_str(), ingredienti_ordinati[j+1].Name.c_str()) > 0){
                //.c_str serve per convertire le stringhe in "C_style stringhe" perchè così richiede la funzione strcmp
                ingrediente_temporaneo = ingredienti_ordinati[j];
                ingredienti_ordinati[j] = ingredienti_ordinati[j+1];
                ingredienti_ordinati[j+1] = ingrediente_temporaneo;
            }
        }
    }
    for (unsigned int i = 0; i < numIngredient; i++){
       prezzo = to_string(ingredienti_ordinati[i].Price); //convert
       ListaIngredienti = ListaIngredienti + ingredienti_ordinati[i].Name + " - '" + ingredienti_ordinati[i].Description + "': " + prezzo + " euro" + "\n";
    }
    return ListaIngredienti;
}

string Pizzeria::Menu() const {

    string Menu;
    string numero_ingredienti;
    string prezzo;
    unsigned int numPizze = NumberPizze();
    for (unsigned int i = 0; i < numPizze; i++){
        numero_ingredienti = to_string(pizze[i].NumIngredients());
        prezzo = to_string(pizze[i].ComputePrice());
        Menu = Menu + pizze[i].Name + " (" + numero_ingredienti + " ingredients): " + prezzo + " euro" + "\n";
    }
    return Menu;
}

void Pizza::AddIngredient(const Ingredient &ingredient) {

    Ingredienti.push_back(ingredient);
}

int Pizza::ComputePrice() const {

    int pizza_price = 0;
    unsigned int numIngredienti = NumIngredients();
    for (unsigned int i = 0; i < numIngredienti; i++){
        pizza_price = pizza_price + Ingredienti[i].Price;
    }
    return pizza_price;
}

void Order::InitializeOrder(int numPizzas) {

    Client = numPizzas + 1000;
}

void Order::AddPizza(const Pizza &pizza) {

    Pizzas.push_back(pizza);
}

const Pizza &Order::GetPizza(const int &position) const {

    return Pizzas[position];
}

int Order::ComputeTotal() const {

    int TOTAL = 0;
    unsigned int numPizze = NumPizzas();
    for ( unsigned int i = 0; i < numPizze; i++){
        TOTAL = TOTAL + Pizzas[i].ComputePrice();
    }
    return TOTAL;
}

}


