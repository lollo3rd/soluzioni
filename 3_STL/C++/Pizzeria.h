#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
  };

  class Pizza {
    public:
      string Name;
      vector<Ingredient> Ingredienti;
    public:
      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const { return Ingredienti.size(); }
      int ComputePrice() const;
  };

  class Order {
    public:
      int Client;
      vector<Pizza> Pizzas;
    public:
      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza &GetPizza(const int& position) const;
      int NumPizzas() const { return Pizzas.size(); }
      int ComputeTotal() const;
  };

  class Pizzeria {
    private:
      vector<Ingredient> ingredients;
      vector<Pizza> pizze;
      vector<Order> orders;
    public:
      unsigned int NumberIngredients() const { return ingredients.size(); }
      unsigned int NumberPizze() const { return pizze.size(); }
      unsigned int NumberOrders() const { return orders.size(); }
      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order &FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
