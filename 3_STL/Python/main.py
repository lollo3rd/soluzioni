class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str, ingredients: []):
        self.Name = name
        self.Ingredienti = ingredients

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredienti.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.Ingredienti)

    def computePrice(self) -> int:
        pizza_price = 0
        for i in range(0, self.numIngredients()):
            pizza_price = pizza_price + self.Ingredienti[i].Price
        return pizza_price


class Order:
    def __init__(self, client: int, pizzas: []):
        self.Client = client
        self.Pizzas = pizzas
    def getPizza(self, position: int) -> Pizza:
        return self.Pizzas[position]

    def initializeOrder(self, numPizzas: int):
        self.Client = numPizzas + 1000

    def addPizza(self, pizza: Pizza):
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.Pizzas)

    def computeTotal(self) -> int:
        TOTAL = 0
        for i in range(0, self.numPizzas()):
            TOTAL = TOTAL + self.Pizzas[i].computePrice()
        return TOTAL


class Pizzeria:
    def __init__(self):
        self.__ingredients = []
        self.__pizze = []
        self.__orders = []

    def NumberIngredients(self) -> int:
        return len(self.__ingredients)

    def NumberPizze(self) -> int:
        return len(self.__pizze)

    def NumberOrders(self) -> int:
        return len(self.__orders)

    def addIngredient(self, name: str, description: str, price: int):
        repeated = False
        ingrediente: Ingredient = Ingredient(name, price, description)
        for i in range(0, self.NumberIngredients()):
            if name == self.__ingredients[i].Name:
                repeated = True
                break
        if repeated == True:
            raise ValueError("Ingredient already inserted")
        else:
            self.__ingredients.append(ingrediente)

    def findIngredient(self, name: str) -> Ingredient:
        flag = 0
        found = False
        for i in range(0, self.NumberIngredients()):
            if name == self.__ingredients[i].Name:
                found = True
                break
            flag = flag + 1
        if found == False:
            raise ValueError("Ingredient not found")
        else:
            return self.__ingredients[flag]

    def addPizza(self, name: str, ingredients: []):
        repeated = False
        pizza: Pizza = Pizza(name, [])
        for i in range(0, len(ingredients)):
            pizza.addIngredient(self.__ingredients[i])
        for i in range(0, self.NumberPizze()):
            if name == self.__pizze[i].Name:
                repeated = True
                break
        if repeated == True:
            raise ValueError("Pizza already inserted")
        else:
            self.__pizze.append(pizza)

    def findPizza(self, name: str) -> Pizza:
        found = False
        flag = 0
        for i in range(0, self.NumberPizze()):
            if name == self.__pizze[i].Name:
                found = True
                break
            flag = flag + 1
        if found == False:
            raise ValueError("Pizza not found")
        else:
            return self.__pizze[flag]

    def createOrder(self, pizzas: []) -> int:
        j = 0
        empty = False
        ordine: Order = Order(0, [])
        for i in range(0, len(pizzas)):
            ordine.addPizza(self.__pizze[i])
        if self.NumberOrders() == 0:
            ordine.initializeOrder(j)
        else:
            while j != self.NumberOrders():
                j = j + 1
            ordine.initializeOrder(j)
        self.__orders.append(ordine)
        number = ordine.Client
        if len(pizzas) == 0:
            empty = True
        if empty == True:
            raise ValueError("Empty order")
        else:
            return number


    def findOrder(self, numOrder: int) -> Order:
        flag = 0
        for i in range(0, self.NumberOrders()):
            if numOrder == self.__orders[i].Client:
                break
            flag = flag + 1
        if len(self.__orders[flag].Pizzas) == 0:
            raise ValueError("Order not found")
        else:
            return self.__orders[flag]

    def getReceipt(self, numOrder: int) -> str:
        if len(self.__orders[numOrder - 1000].Pizzas) == 0:
            raise ValueError("Order not found")
        else:
            Receipt: str = ""
            total = str(self.__orders[numOrder - 1000].computeTotal())
            for i in range(0, len(self.__orders[numOrder - 1000].Pizzas)):
                prezzo_pizza = str(self.__orders[numOrder - 1000].Pizzas[i].computePrice())
                Receipt = Receipt + ("- " + self.__orders[numOrder - 1000].Pizzas[i].Name
                                     + ", " + prezzo_pizza + " euro" + "\n")
            Receipt = Receipt + "  TOTAL: " + total + " euro" + "\n"
            return Receipt

    def listIngredients(self) -> str:
        Listaingredienti: str = ""
        ingredienti_ordinati: [] = sorted(self.__ingredients, key=lambda ing: ing.Name)
        for ing in ingredienti_ordinati:
            Listaingredienti = Listaingredienti + (ing.Name + " - '"
                                                   + ing.Description + "': "
                                                   + str(ing.Price) + " euro" + "\n")
        return Listaingredienti

    def menu(self) -> str:
        Menu: str = ""
        for i in range(0, self.NumberPizze()):
            Menu = Menu + (self.__pizze[i].Name
                           + " (" + str(self.__pizze[i].numIngredients())
                           + " ingredients): " + str(self.__pizze[i].computePrice()) +
                           " euro" + "\n")
        return Menu

